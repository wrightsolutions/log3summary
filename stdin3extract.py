#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

import re
from sys import argv,exit,stdin
from os import getenv as osgetenv
from os import path,stat
#from time import gmtime
from datetime import datetime as dt

""" Count [and report] on pattern matches in the file piped into stdin
Takes standard (uncompressed) system files from /var/log/
Second argument is a pre category (think prefixing [\d+])
Example1: cat /var/log/secure | python3 ./stdin3extract.py 'POSSIBLE' 'sshd'
# Next example makes use of default PATTERN_DEFAULT internally
Example2: cat /var/log/secure | python3 ./stdin3extract.py '' 'sshd'
# Next example uses final arg 'compact' which means suppress printing anything prior to precat
Example3: cat /var/log/secure | python3 ./stdin3extract.py '' 'sshd' 'compact'
# If not bothered about the date/time part of each line then final arg 'compact' suggested above
# Just want a count? See next example...
Example4: cat /var/log/secure | python3 ./stdin3extract.py '' 'sshd' | fgrep 'lines returned from'
# Want to check that if secure log is filled with mysql login attempts?
Example5: cat /var/log/secure | python3 ./stdin3extract.py '' 'sshd' 'compact' | fgrep 'mys'
"""

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

""" Next verbosity_global = 1 is the most likely outcome
unless the user specifically overrides it by setting
the environment variable PYVERBOSITY
"""
if PYVERBOSITY is None:
	verbosity_global = 1
elif 0 == PYVERBOSITY:
	# 0 from env means 0 at global level
	verbosity_global = 0
elif PYVERBOSITY > 1:
	# >1 from env means same at global level
	verbosity_global = PYVERBOSITY
else:
	verbosity_global = 1


#from string import ascii_letters,punctuation,printable
from string import printable
SET_PRINTABLE = set(printable)

progfull = argv[0].strip()
from os import path
progbase = path.basename(progfull)
progname = progbase.split('.')[0]
progdir = path.dirname(progbase)

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

COLON=chr(58)
SPACER=chr(32)
PARENTH_OPEN = chr(40)
PARENTH_CLOSE = chr(41)
# Above are () and next we define {}
BRACES_OPEN = chr(123)
BRACES_CLOSE = chr(125)
# Now for type that appear mostly in system files []
BRACK_OPEN = chr(91)
BRACK_CLOSE = chr(93)

PATTERN_DEFAULT = '[uU]ser\s*\w+\s*from'

RE_PATTERN_DEFAULT = re.compile(PATTERN_DEFAULT)
RE_BRACKETED_NUM = re.compile(r"{0}\d*{1}".format(BRACK_OPEN,BRACK_CLOSE))
RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')
RE_BRACKETED_LOOSE = re.compile(r"\s*\[.*\]".format(BRACK_OPEN,BRACK_CLOSE))
OPEN_CLOSE_BRACKETS = '(?P<open_close_brackets>\[.*\])'

SSHD_CONFIG_USER="""
DenyUsers mysql* postgres*

Match User postgres,mysql
	X11Forwarding no
	AllowTcpForwarding no
	PermitTTY no
	PasswordAuthentication no
	ForceCommand /bin/false
"""

def regexer_precat(precat=None,bracket_suffix=True):
	if precat is None:
		return None
	if bracket_suffix is True:
		regex_p = re.compile(r"{0}\{1}[0-9]*\{2}".format(precat,
						BRACK_OPEN,BRACK_CLOSE))
	else:
		regex_p = re.compile(precat)
	return regex_p


def lines_compact(lines_full,precat=None,verbosity=0):
	if precat is None or len(precat) < 1:
		return lines_full

	re_precat = None
	if set(precat).issubset(SET_PRINTABLE):
		# precat (pre-category) regex required
		#re_precat = re.compile(precat)
		re_precat = regexer_precat(precat)
	else:
		pass

	if re_precat is None:
		return lines_full

	lines = []

	for line in lines_full:
		rline = ''
		matched = re_precat.search(line)
		if matched:
			try:
				rline = line[matched.end():]
			except:
				pass
		if rline.lstrip().startswith(COLON):
			rline = rline.lstrip("{0}{1}".format(COLON,SPACER))
		lines.append(rline)

	return lines


def lines_iter_counter(lines_iter,pattern=PATTERN_DEFAULT,precat=None,verbosity=0):
	if pattern is None or len(pattern.strip()) < 2:
		pattern = PATTERN_DEFAULT

	if pattern == PATTERN_DEFAULT:
		re_pattern = RE_PATTERN_DEFAULT
	elif set(pattern).issubset(SET_PRINTABLE):
		try:
			re_pattern = re.compile(pattern)
		except:
			re_pattern = None
	else:
		re_pattern = None

	re_precat = None
	if re_pattern is None:
		pass
	elif precat is None or len(precat) < 1:
		pass
	elif set(precat).issubset(SET_PRINTABLE):
		# precat (pre-category) regex required
		#re_precat = regexer_precat(precat)
		# Enable above line in future but for now 2nd arg False
		re_precat = regexer_precat(precat,False)
	else:
		pass

	match_lines = []

	while re_pattern is not None:
		try:
			line = next(lines_iter)
		except StopIteration:
			break

		sline = line.rstrip()
		if re_precat:
			rline = ''
			matched = re_precat.search(line)
			if matched:
				try:
					rline = sline[matched.end():]
				except:
					pass
			if len(rline) > 1:
				# Have something still in remainder (rline)
				if re_pattern.search(rline):
					match_lines.append(sline)
		else:
			# Simpler branch as no precat to consider
			if re_pattern.search(line):
				match_lines.append(sline)

	return match_lines


if __name__ == '__main__':


	pattern = None
	precat = None
	mode = 'full'
	if len(argv) > 1:
		pattern = argv[1]
		if len(argv) > 2:
			precat = argv[2]
			if len(argv) > 3:
				mode = argv[3]

	# Prefer iter(stdin.readline, '') instead of iter(stdin.readlines())
	stdin_iter = iter(stdin.readline, '')
	matching_lines = lines_iter_counter(stdin_iter,pattern,precat,verbosity_global)
	#matching_lines = lines_iter_counter(stdin_iter,pattern=PATTERN_DEFAULT,None)
	mcount = len(matching_lines)

	if verbosity_global > 1:
		print("{0} matching lines returned from lines_iter work".format(mcount))

	if 'compact' == mode or 'concise' == 'mode':
		output_lines = lines_compact(matching_lines,precat,verbosity_global)
	else:
		#output_lines = matching_lines[:]
		output_lines = matching_lines

	for line in output_lines:
		print(line)

	if 'sshd' == precat:
		for line in SSHD_CONFIG_USER.splitlines():
			print(line)

