# Syslog summariser - Python 3 version



## Syslog summariser 'compare' arg to syslog3selected

cat input/*.log >> /tmp/syslog
python3 ./syslog3selected.py 'compare'

Based on rules in rules/select.rules
or 
/etc/syslog-summary/select.rules

will summarise the count of the patterns and 'compare' timings of two
classes Selecting and SelectingSimple in completing the task


## Syslog summariser 'compare' arg to syslog3selected - output extract

```
[re.compile('cron: \\(CRON\\) INFO.*pidfile')]
Elapsed time for process_lines(): 0.061539
Elapsed time for summarize_selected: 0.064046
5
6603
       9 debian /usr/sbin/cron: (CRON) INFO (pidfile fd = 3)
       3 SP2128 /usr/sbin/cron: (CRON) INFO (pidfile fd = 3)
       1 rsc20110824squeeze64sml /usr/sbin/cron: (CRON) INFO (pidfile fd = 3)
      12 rsc20111009squeeze64sml /usr/sbin/cron: (CRON) INFO (pidfile fd = 3)
       1 rsc20111031squeeze64sml /usr/sbin/cron: (CRON) INFO (pidfile fd = 3)
 2424 items in counts dictionary - printing is dependent on order_list
Summarizing /tmp/syslog
       0 Lines skipped (already processed)
SummarizeFileSelecting class having selected linecount of 5 from possible 6603
len(counts_dict)=2424
2018-12-21T21:06:03.234397 Completed class_word=Selecting *** end ***
```

