#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2013 Gary Wright wrightsolutions.co.uk/contact
#           © 2008-2009, David Paleino <d.paleino@gmail.com>
#           © 2001-2008, Tommi Virtanen <tv@debian.org>
#           © 1998-2000, Lars Wirzenius <liw@iki.fi>
# 
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 3 of the License, or
#      (at your option) any later version.
#      
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#      
#      You should have received a copy of the GNU General Public License
#      along with this program; if not, write to the Free Software
#      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#      MA 02110-1301, USA.
 
"""Summarize the contents of a syslog log file.

The syslog(3) service writes system log messages in a certain format:

	Jan 17 19:21:50 zeus kernel: klogd 1.3-3, log source = /proc/kmsg started.

This program summarizes the contents of such a file, by displaying each
unique (except for the time) line once, and also the number of times such
a line occurs in the input. The lines are displayed in the order they occur
in the input.

Lars Wirzenius <liw@iki.fi>
Tommi Virtanen <tv@debian.org>
David Paleino <d.paleino@gmail.com>

Fork of version = "1.14" to become a part object-oriented chimera, with no performance penalty.
Purpose is to facilitate specialisation (via later subclassing of SummarizeFile class) and,
if practical, import friendly way of getting to summary lines.
Python 2 versions will use version = "2.0"
Python 3 versions will use version = "3.0"
Two newer functions summarize_selected() and summarize_stateful() are designed to delegate
printing to the caller. If you are using a harness or importing this file as a module
then you may want to follow the calls to these functions with (example):
for rest in summarized.order_list:
   print("{0:8d} {1:s}".format(summarized.counts_dict[rest], rest),end='')
See also the report_lines property of the SummarizeFile and subclasses.
"""
version = '3.0'

import sys, re, getopt, string
from gzip import open as gzopen
from hashlib import sha1
from optparse import OptionParser


class SummarizeFile(object):
	""" SummarizeFile parent class. Can be used directly but subclasses are
	also implemented later in this file.
	Design decision: ignored_count in here to fit with traditional script call
	Design decision: patterns as instance variables rather than class variables.
	If you prefer strict OO thinking or are creating more than a handful of 
	instances then the argument for having datepats and similar as class variables
	gets stronger.
	"""

	def __init__(self, logfile, hashobj, linecount=0):
		self._logfile = logfile
		self._logfile_encoding = logfile.encoding
		self._hashobj = hashobj
		self._linecount = linecount
		self._counts_dict = {}
		self._order_list = []
		self._debug = False
		self._repeat = False
		self.datepats = [re.compile(
				r"^(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [ 0-9][0-9] [ 0-9][0-9]:[0-9][0-9]:[0-9][0-9] "
				),
				 re.compile(
				r"^(Mon|Tue|Wed|Thu|Fri|Sat|Sun) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [ 0-9][0-9][0-9][0-9]:[0-9][0-9] "
				),
				 re.compile(
				r"^(Mon|Tue|Wed|Thu|Fri|Sat|Sun) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) [ 0-9][0-9][0-9][0-9]:[0-9][0-9]:[0-9][0-9] "
				),]
		self.datepats_ordered = self.datepats[:]
		self.pidpat = re.compile(r"^([^ ]* [^ ]*)\[[0-9][0-9]*\]: ")
		self.repeatpat = re.compile(r"^[^ ]* last message repeated (\d+) times$")
		self._report_lines = []
		# Ignored count and its property methods could be in SummarizeFileIgnoring only
		self._ignored_count = 0

	def logfile_get(self):
		return self._logfile

	logfile = property(logfile_get)

	def logfile_encoding_get(self):
		return self._logfile_encoding

	logfile_encoding = property(logfile_encoding_get)

	def hashobj_get(self):
		return self._hashobj

	hashobj = property(hashobj_get)
	
	def linecount_get(self):
		return self._linecount

	linecount = property(linecount_get)

	def counts_dict_get(self):
		return self._counts_dict

	counts_dict = property(counts_dict_get)

	def order_list_get(self):
		return self._order_list

	order_list = property(order_list_get)

	def debug_get(self):
		return self._debug

	def debug_set(self,debug):
		self._debug = debug

	debug = property(debug_get,debug_set)

	def repeat_get(self):
		return self._repeat

	def repeat_set(self,repeat):
		self._repeat = repeat

	repeat = property(repeat_get,repeat_set)

	def report_lines_get(self):
		return self._report_lines

	report_lines = property(report_lines_get)

	def ignored_count_get(self):
		return self._ignored_count

	ignored_count = property(ignored_count_get)

	def split_date(self,line):
		for pat in self.datepats_ordered:
			m = pat.match(line)
			if m:
				return line[:m.end()], line[m.end():]
			print("line has bad date", "<" + string.rstrip(line) + ">")
		return None, line

	def split_date_rest(self,line,remove_regex=None):
		for pat in self.datepats_ordered:
			re_match = pat.match(line)
			if re_match:
				re_match_end = re_match.end()
				rest = line[re_match_end:]
				if remove_regex:
					found = remove_regex.search(rest)
					if found:
						rest = found.group(1) + ": " + rest[found.end():]
				return line[:re_match_end], rest
			print("line has bad date", "<" + string.rstrip(line) + ">")
		return None, line

	def split_date_ordering(self,line,datepats_list):
		pat_index = 0
		for pat in datepats_list:
			re_match = pat.match(line)
			if re_match:
				#print pat_index
				if pat_index > 0:
					self.datepats_ordered[0]=self.datepats_ordered[pat_index]
					self.datepats_ordered[pat_index]=self.datepats_ordered[0]
					# Now first datepats position occupied by important pattern
				return line[:re_match.end()], line[re_match.end():]
			print("line has bad date", "<" + string.rstrip(line) + ">")
			pat_index += 1
		return None, line
	
	def process_lines(self):

		line = self._logfile.readline()
		previous = None
		foo=0

		while line:
			self._hashobj.update(line.encode(self._logfile_encoding))
			self._linecount += 1

			date, rest = self.split_date(line)
			if date:
				found = self.pidpat.search(rest)
				if found:
					rest = found.group(1) + ": " + rest[found.end():]

			count = 1
			repeated = None
			if self.repeat:
				repeated = self.repeatpat.search(rest)
			if repeated and previous:
				count = int(repeated.group(1))
				rest = previous

			if rest in self._counts_dict:
				self._counts_dict[rest] = self._counts_dict[rest] + count
			else:
				assert count == 1
				self._counts_dict[rest] = count
				self._order_list.append(rest)

			if not repeated:
				previous = rest
			line = self._logfile.readline()

		return self._linecount

	def __str__(self):
		return_string = "{0} class having internal linecount of {1}".format(
			self.__class__.__name__,self._linecount)
		return return_string


class SummarizeFileIgnoring(SummarizeFile):
	""" Subclass supports ignore patterns """

	def __init__(self, *args, **kwargs):
		super(SummarizeFileIgnoring, self).__init__(*args, **kwargs)
		self._ignore_pats = []
		# superclass: self._ignored_count = 0

	def ignore_pats_get(self):
		return self._ignore_pats

	def ignore_pats_set(self,ignore_pats):
		self._ignore_pats = ignore_pats

	ignore_pats = property(ignore_pats_get,ignore_pats_set)

	def should_be_ignored(self,line):
		for pat in self._ignore_pats:
			if pat.search(line):
				return 1
		return 0

	def process_lines(self):

		line = self._logfile.readline()
		previous = None
	#       print "BEFORE-while: %s" % (self._hashobj).hexdigest()
		foo=0

		while line:
			self._hashobj.update(line.encode(self._logfile_encoding))
			self._linecount += 1

			if self.should_be_ignored(line):
				self._ignored_count += 1
				if DEBUG:
					print("Ignoring: %s" % line)
				line = self._logfile.readline()
				if not line:
					break
		
			if self._linecount < 2:
				date, rest = self.split_date_ordering(line,self.datepats_ordered)
				# First few lines are used also in an attempt to put
				# the most relevant datepat at front of the list
			else:
				date, rest = self.split_date(line)
			if date:
				found = self.pidpat.search(rest)
				if found:
					rest = found.group(1) + ": " + rest[found.end():]

			count = 1
			repeated = None
			if self.repeat:
				repeated = self.repeatpat.search(rest)
			if repeated and previous:
				count = int(repeated.group(1))
				rest = previous

			if rest in self._counts_dict:
				self._counts_dict[rest] = self._counts_dict[rest] + count
			else:
				assert count == 1
				self._counts_dict[rest] = count
				self._order_list.append(rest)

			if not repeated:
				previous = rest
			line = self._logfile.readline()

		return self._linecount


class SummarizeFileTwoCount(SummarizeFile):
	""" Subclass has extra property linecount_selected and that makes it TwoCount """

	def __init__(self, *args, **kwargs):
		super(SummarizeFileTwoCount, self).__init__(*args, **kwargs)
		self._select_pats = []
		self._linecount_selected = 0

	def select_pats_get(self):
		return self._select_pats

	def select_pats_set(self,select_pats):
		self._select_pats = select_pats

	select_pats = property(select_pats_get,select_pats_set)

	def linecount_selected_get(self):
		return self._linecount_selected

	linecount_selected = property(linecount_selected_get)

	def __str__(self):
		return_string = "{0} class having selected linecount of {1}".format(
			self.__class__.__name__,self._linecount_selected)
		return_string = "{0} from possible {1}".format(return_string,self._linecount)
		return return_string


class SummarizeFileSelecting(SummarizeFileTwoCount):
	""" Subclass supports select patterns """

	def __init__(self, *args, **kwargs):
		super(SummarizeFileSelecting, self).__init__(*args, **kwargs)

	def process_lines(self):

		line = self._logfile.readline()
		previous = None
		previous_match = False
	#       print "BEFORE-while: %s" % (self._hashobj).hexdigest()

		while line:
			self._hashobj.update(line.encode(self._logfile_encoding))
			self._linecount += 1

			if self._linecount < 2:
				date, rest = self.split_date_ordering(line,self.datepats_ordered)
				# First few lines are used also in an attempt to put
				# the most relevant datepat at front of the list
				if date:
					found = self.pidpat.search(rest)
					if found:
						rest = found.group(1) + ": " + rest[found.end():]
			else:
				date, rest = self.split_date_rest(line,self.pidpat)

			count = 1
			repeated = None
			if self.repeat:
				repeated = self.repeatpat.search(rest)
			if repeated and previous:
				count = int(repeated.group(1))
				rest = previous

			re_match = False
			if rest in self._counts_dict:
				self._counts_dict[rest] = self._counts_dict[rest] + count
			else:
				assert count == 1
				if repeated and previous and previous_match:
					self._linecount_selected += 1
					self._order_list.append(rest)
				elif self.select_pats is None:
					raise ValueError('No patterns available in select_pats to select')
				else:
					try:
						for pat in self.select_pats:
							re_match = pat.search(rest)
							if re_match:
								self._linecount_selected += 1
								self._order_list.append(rest)
								# End the search at the first positive hit
								break
					except TypeError:
						print('For loop over select_pats encountered TypeError')
						raise
					except:
						print('For loop over select_pats encountered error')
						raise
				self._counts_dict[rest] = count

			if not repeated:
				previous = rest
				previous_match = re_match
			line = self._logfile.readline()

		return self._linecount

	def __str__(self):
		return_string = "{0} class having selected linecount of {1}".format(
			self.__class__.__name__,self._linecount_selected)
		return_string = "{0} from possible {1}".format(return_string,self._linecount)
		return return_string


class SummarizeFileSelectingSimple(SummarizeFileTwoCount):
	""" Subclass supports select patterns """

	def __init__(self, *args, **kwargs):
		super(SummarizeFileSelectingSimple, self).__init__(*args, **kwargs)
		self.repeatpat = re.compile('last message repeated')

	def process_lines(self):
		""" This variant may run slightly faster than a more traditional
		implementation based on an assumption that the selections will
		result in fewer than half of the original lines matching.
		Two decisions here: Only selected lines result in a counts_dict update
		Have chosen to defer the check for 'repeatpat' until later in the logic
		based on this assumption.
		"""
		line = self._logfile.readline()
		previous = None
		previous_match = False
	#       print "BEFORE-while: %s" % (self._hashobj).hexdigest()

		while line:
			self._hashobj.update(line.encode(self._logfile_encoding))
			self._linecount += 1

			if self._linecount < 2:
				date, rest = self.split_date_ordering(line,self.datepats_ordered)
				# First few lines are used also in an attempt to put
				# the most relevant datepat at front of the list
				if date:
					found = self.pidpat.search(rest)
					if found:
						rest = found.group(1) + ": " + rest[found.end():]
			else:
				date, rest = self.split_date_rest(line,self.pidpat)

			repeated = None
			re_match = False
			if rest in self._counts_dict:
				self._counts_dict[rest] = self._counts_dict[rest] + 1
			else:
				if self.repeat:
					repeated = self.repeatpat.search(rest)
					if repeated:
						#print(rest)
						rest = previous

				if repeated and previous and previous_match:
					self._linecount_selected += 1
				else:
					for pat in self.select_pats:
						re_match = pat.search(rest)
						if re_match:
							self._linecount_selected += 1
							self._order_list.append(rest)
							self._counts_dict[rest] = 1
							# End the search at the first positive hit
							break

			if not repeated:
				previous = rest
				previous_match = re_match
			line = self._logfile.readline()

		return self._linecount

	def __str__(self):
		return_string = "{0} class having selected linecount of {1}".format(
			self.__class__.__name__,self._linecount_selected)
		return_string = "{0} from possible {1}".format(return_string,self._linecount)
		return return_string


class SummarizeFileMixed(SummarizeFileTwoCount):
	""" Subclass has a touch of premature optimisation that may prove useful where
	an individual log file contains multiple date formats.
	"""

	def __init__(self, *args, **kwargs):
		super(SummarizeFileMixed, self).__init__(*args, **kwargs)

	def process_lines(self):

		line = self._logfile.readline()
		previous = None
	#       print "BEFORE-while: %s" % (self._hashobj).hexdigest()
		foo=0

		while line:
			self._hashobj.update(line.encode(self._logfile_encoding))
			self._linecount += 1

			"""
			if should_be_ignored(line):
				ignored_count += 1
				if self.debug:
					print "Ignoring: %s" % line
				line = self._logfile.readline()
			"""
		
			if self._linecount < 2:
				date, rest = self.split_date_ordering(line,self.datepats_ordered)
				# First few lines are used also in an attempt to put
				# the most relevant datepat at front of the list
			else:
				date, rest = self.split_date(line)
			if date:
				found = self.pidpat.search(rest)
				if found:
					rest = found.group(1) + ": " + rest[found.end():]

			count = 1
			repeated = None
			if self.repeat:
				repeated = self.repeatpat.search(rest)
			if repeated and previous:
				count = int(repeated.group(1))
				rest = previous

			if rest in self._counts_dict:
				self._counts_dict[rest] = self._counts_dict[rest] + count
			else:
				assert count == 1
				self._counts_dict[rest] = count
				self._order_list.append(rest)

			if not repeated:
				previous = rest
			line = self._logfile.readline()

		return self._linecount


def io_error(err, filename, die=True):
	"""Prints a nice error message, i.e. Tracebacks are ugly to end users"""
	import os, errno, traceback
	num = err.errno
	# DEBUG && die ensures that if it's a non-fatal exception, we don't
	# show all the traceback mess...
	try:
		if DEBUG:
			if die:
				traceback.print_exc(file=sys.stderr)
			else:
				print("[E] %s [%s(%s) - %s]" % (os.strerror(num),
								errno.errorcode[num], num, filename))
	except NameError as e:
		pass
	if die:
		sys.exit(1)

def isfile_and_positive_size(target_path_given):
    from os import path as pth
    target_path = target_path_given.strip()
    try:
	    if not pth.isfile(target_path):
		    return False
    except IOError as e:
	    return False

    from os import stat
    try:
	    if not stat(target_path).st_size:
		    return False
    except IOError as e:
	    return False
    return True

def append_regex_if_valid(regex,list_append_to=None,debug=False):
	""" First argument (regex) is compiled and failures caught.
	Second argument (if given) is a list of regex to which any
	successful compile of regex will be appended (in place)
	If you do not supply a second argument then you either get
	the compiled regex back or None in case of compile failure.
	Return is a tuple. First element will be None when compilation failed
	"""
	try:
		re_compiled = re.compile(regex)
		if debug:
			print('Regex compilation success.')
	except Exception as e:
		if debug:
			print('Regex compilation failure: %s' % regex)
		re_compiled = None
	if re_compiled and list_append_to is not None:
		try:
			list_append_to.append(re_compiled)
		except AttributeError as e:
			pass
	return (re_compiled,list_append_to)

def read_regex(regex_filename,debug=False):
	"""Reads regex rules/patterns from the given filename"""
	rule_list = []
	try:
		with open(regex_filename, "r") as regex_file:
			for line in regex_file:
				rule = line.strip()
				if not rule.startswith("#"):
					re_compiled,re_list = append_regex_if_valid(rule,rule_list,debug)
					if re_compiled is not None:
						rule_list = re_list
	except IOError as e:
		io_error(e, regex_filename, False)
	if debug:
		print('read_regex returning list of length %s' % len(rule_list))
		print('from regex rules file %s' % regex_filename)
		print(rule_list)
	return rule_list

def read_patterns(filename):
	"""Reads patterns to ignore from file specified by -i | --ignore="""
	pats = []
	try:
		f = open(filename, "r")
	except IOError as e:
		io_error(e, filename, False)
		return []
	for line in f:
		rule = line.strip()
		if rule[0:1] == "#":
			continue
		else:
			pats.append(re.compile(rule))
	f.close()
	return pats

def read_states(filename):
	"""Reads the previous state saved into the argument of -s | --state="""
	states = {}
	if not filename:
		return states
	try:
		f = open(filename, "r")
	except IOError as e:
		io_error(e, filename, False)
		return states
	for line in f:
		fields = line.split()
		try:
			lines = int(fields[1])
			hashtext = fields[2]
		except ValueError as e:
			pass
		else:
			if lines > 0:
				states[fields[0]] = (lines, hashtext)
	f.close()
	return states

def save_states(filename, states):
	if not filename:
		return
	try:
		f = open(filename, "w")
	except IOError as e:
		io_error(e, filename, True)
	for filename in states.keys():
		try:
			value = states[filename]
		except KeyError as e:
			continue
		f.write("%s %d %s\n" % (filename, value[0], value[1]))
	f.close()

def is_gzipped(filename):
	"""Returns True if the filename is a gzipped compressed file"""	
	try:
		import magic
		ms = magic.open(magic.MAGIC_NONE)
		ms.load()
		if re.search("^gzip compressed data.*", ms.file(filename)):
			return True
		else:
			return False
	except:
		from os.path import splitext

		try:
			if not QUIET:
				fallback_message = "Using fallback detection... please install"
				fallback_message += " python-magic for better gzip detection."
				print(fallback_message)
		except NameError as e:
			pass
		
		if splitext(filename)[1] == ".gz":
			return True
		else:
			return False

def tuple_from_state(state=None,logfile=None):
	""" Use state file contents to decide to start
	at beginning or continue from a stored line reference.
	The shaobj.hexdigest() != oldsha test is a simple way,
	of detecting if, a saved state seems inconsistent with,
	the contents of the file with that name currently.
	"""
	if not state:
		return (0,sha1(),[])
	linecount = 0
	shaobj = sha1()
	report_lines = []

	oldlines, oldsha = state
	# NOT limited by oldlines:- for line in logfile: shaobj.update(line)	
	for _ in range(oldlines):
		line = logfile.readline()
		shaobj.update(line.encode(logfile.encoding))
	report_lines.append("OLD-new: %s" % shaobj.hexdigest())
	report_lines.append("OLD-file: %s" % oldsha)
	if shaobj.hexdigest() != oldsha:
		#logfile.seek(0, 0)
		logfile.seek(0)
		shaobj = sha1()
	else:
		linecount = oldlines
	return (linecount,shaobj,report_lines)


class ElapsedIfDebug(object):
	def __init__(self, f):
		from time import time as tm
		self.f = f
		self.tm_start = tm()

	def __call__(self, *args, **kwargs):
		""" No support for 'debug' given as a positional argument,
		as this decorator is designed to be generic.
		"""
		from time import time as tm
		f_return = self.f(*args, **kwargs)
		if not 'debug' in kwargs:
			return f_return
		f_debug = kwargs['debug']
		if not f_debug:
			return f_return
		f_elapsed = tm()-self.tm_start
		f_name = self.f.__name__
		print("Elapsed time for %s: %8f" % (f_name,f_elapsed))
		return f_return

@ElapsedIfDebug
def summarize_selected(filename, state=None,
		       regex_list=None,debug=False,class_word=None):
	""" Third argument is key to what happens here.
	regex_list=None will attempt to find the regex in a local .rules file
	regex_list=[] (empty list) will cause this function to instanciate ...
	superclass SummarizeFile and fit with traditional usage (non selecting)
	regex_list populated with regexs instanciates SummarizeFileSelecting and
	is the typical usage of this function.
	"""
	ignored_count = 0
	
	# If the file is a gzipped log, open it
	# using the proper function from the gzip
	# module.
	try:
		if is_gzipped(filename):
			logfile = gzopen(filename, "rb")
		else:
			logfile = open(filename, "r")
	except IOError as e:
		print('IO Error during summarize_selected.')
		return None

	linecount = 0
	shaobj = sha1()
	if state:
		linecount, shaobj, report_lines = tuple_from_state(
			state,logfile)
		if debug:
			for repline in report_lines:
				print(repline)

	""" When class_word is None (or not supplied) we use SelectingSimple
	where we have / can obtain a list of suitable regexs. Fallback to
	Mixed otherwise.
	When class_word is supplied, we attempt to honour that request where
	practical
	"""
	summarized = None
	if regex_list is None:
		select_filename = '/etc/syslog-summary/select.rules'
		if isfile_and_positive_size(select_filename):
			regex_list = read_regex(select_filename,debug)
		elif isfile_and_positive_size('rules/select.rules'):
			regex_list = read_regex('rules/select.rules',debug)
		else:
			pass
			
	if regex_list and len(regex_list) > 0:
		if class_word is None:
			summarized = SummarizeFileSelectingSimple(logfile, shaobj, linecount)
			summarized.select_pats = regex_list
	elif class_word is None:
		# Fallback to traditional summarize (does not make use of select.rules)
		summarized = SummarizeFileMixed(logfile, shaobj, linecount)
	if summarized is None:
		if class_word is not None:
			class_word_lower = class_word.lower()
		if class_word_lower == 'mixed':
			summarized = SummarizeFileMixed(logfile, shaobj, linecount)
		elif class_word_lower == 'selecting':
			summarized = SummarizeFileSelecting(logfile, shaobj, linecount)
			summarized.select_pats = regex_list
		elif class_word_lower == 'selectingsimple':
			summarized = SummarizeFileSelectingSimple(logfile, shaobj, linecount)
			summarized.select_pats = regex_list
		elif regex_list and len(regex_list) > 0:
			summarized = SummarizeFileSelectingSimple(logfile, shaobj, linecount)
			summarized.select_pats = regex_list
		else:
			summarized = SummarizeFileMixed(logfile, shaobj, linecount)

	summarized.debug = debug
	summarized.repeat = True
	""" Above .repeat property set True as when using select rules, we are never interested
	in repeat type messages being printed / returned so have them gobbled by repeat=True.
	If a Selecting class chooses not to implement code to test a .repeat property then
	they would still not appear in printed / returned output unless caught in a supplied rule
	"""
	if debug:
		from time import time as tme
		tme_start = tme()
	process_lines_ret = summarized.process_lines()
	logfile.close()
	if debug:
		process_elapsed = tme()-tme_start
		print("Elapsed time for process_lines(): %8f" % process_elapsed)
	summarized.report_lines.insert(0,"Summarizing %s" % filename)
	summarized.report_lines.append("%8d Lines skipped (already processed)" % linecount)
	if debug:
		summarized.report_lines.append("%s" % summarized)
		if regex_list and len(regex_list) > 0:
			if summarized.select_pats:
				summarized.report_lines.append("len(counts_dict)=%d" %
							       len(summarized.counts_dict))
	return summarized

@ElapsedIfDebug
def summarize_stateful(filename, state=None, debug=False,
		       repeat=False,class_word=None):
	ignored_count = 0
	
	# If the file is a gzipped log, open it
	# using the proper function from the gzip
	# module.
	try:
		if is_gzipped(filename):
			file = gzopen(filename, "rb")
		else:
			file = open(filename, "r")
	except IOError as e:
		print('IO Error during summarize_stateful.')
		return None

	linecount = 0
	shaobj = sha1()
	if state:
		linecount, shaobj, report_lines = tuple_from_state(
			state,file)
		if debug:
			for repline in report_lines:
				print(repline)

	class_word_lower = class_word
	if class_word is not None:
		class_word_lower = class_word.lower()
	if class_word_lower == 'mixed':
		summarized = SummarizeFileMixed(file, shaobj, linecount)
	elif class_word_lower == 'selecting':
		summarized = SummarizeFileSelecting(file, shaobj, linecount)
	elif class_word_lower == 'selectingsimple':
		summarized = SummarizeFileSelectingSimple(file, shaobj, linecount)
	else:
		summarized = SummarizeFile(file, shaobj, linecount)
	summarized.debug = debug
	summarized.repeat = repeat
	if debug:
		from time import time as tme
		tme_start = tme()
	process_lines_ret = summarized.process_lines()
	file.close()
	if debug:
		from time import time as tme
		process_elapsed = tme()-tme_start
		print("Elapsed time for process_lines(): %8f" % process_elapsed)
	summarized.report_lines.insert(0,"Summarizing %s" % filename)
	summarized.report_lines.append("%8d Lines skipped (already processed)" % linecount)
	if debug:
		summarized.report_lines.append("%s" % summarized)
	return summarized

def summarize(filename, states={}):
	ignored_count = 0
	if not QUIET:
		print("Summarizing %s" % filename)

	if DEBUG:
		from time import time
		c0 = time()
	
	# If the file is a gzipped log, open it
	# using the proper function from the gzip
	# module.
	try:
		if is_gzipped(filename):
			file = gzopen(filename, "rb")
		else:
			file = open(filename, "r")
	except IOError as e:
		print('IO Error during summarize.')
		io_error(e, filename, True)

	if DEBUG:
		c1 = time()
		elapsed1 = c1-c0
		print("Elapsed1 time: %8f" % elapsed1)

	linecount = 0
	shaobj = sha1()
	if filename in states:
		linecount, shaobj, report_lines = tuple_from_state(
			states[filename],file)
		if DEBUG:
			for repline in report_lines:
				print(repline)

	if not QUIET:
		print("%8d Lines skipped (already processed)" % linecount)

	if DEBUG:
		c2 = time()
		elapsed2 = c2-c1
		print("Elapsed2 time: %8f" % elapsed2)
		
	summarized = None
	if ignore_pats:
		summarized = SummarizeFileIgnoring(file, shaobj, linecount)
		summarized.ignore_pats = ignore_pats
	else:
		summarized = SummarizeFile(file, shaobj, linecount)
	summarized.debug = DEBUG
	summarized.repeat = REPEAT

	if DEBUG:
		c3 = time()
		elapsed3 = c3-c2
		print("Elapsed3 time: %8f" % elapsed3)

	process_lines_ret = summarized.process_lines()
	file.close()

	if DEBUG:
		c4 = time()
		elapsed4 = c4-c3
		print("Elapsed4 time: %8f" % elapsed4)


	# if not QUIET:
	# 	print "%8d Lines scanned" % process_lines_ret
	# 	print summarized

#	print "TOT-lines: %d" % linecount
#	print "TOT-ignor: %d" % ignored_count
#	print "AFTER-while: %s" % shaobj.hexdigest()
#	print foo
	states[filename] = (summarized.linecount + summarized.ignored_count,
			    summarized.hashobj.hexdigest())
#	print states
	
	#print process_lines_ret
	#print len(summarized.order_list)
	#len() above matches input lines. len() below is after deduplication so less.
	#print len(summarized.counts_dict)
	
	if QUIET and summarized.order_list:
		print("Summarizing %s" % filename)
	if not QUIET or summarized.order_list:
		print("%8d Patterns to ignore" % len(ignore_pats))
		print("%8d Ignored lines" % ignored_count)
	if DEBUG:
		c5 = time()
		elapsed5 = c5-c4
		print("Elapsed5 time: %8f" % elapsed5)
	for rest in summarized.order_list:
		print("%8d %s" % (summarized.counts_dict[rest], rest), end='')
	if not QUIET or summarized.order_list:
		print()

	return summarized


def main():
	global ignore_pats, IGNORE_FILENAME, STATE_FILENAME, REPEAT, QUIET, DEBUG

	parser = OptionParser(usage="%prog [options] <logfile> [<logfile> ...]",
	                      version="%%prog %s" % version,
	                      description="Summarize the contents of a syslog log file")
	parser.add_option("-i", "--ignore", dest="ignorefile", default="/etc/syslog-summary/ignore.rules",
	                  help="read regular expressions from <file>, and ignore lines in the <logfile> that match them",
	                  metavar="<file>")
	parser.add_option("-s", "--state", dest="statefile",
	                  help="read state information from <file> (see the man page)",
	                  metavar="<file>")
	parser.add_option("-r", "--repeat", action="store_true", dest="repeat", default=False,
	                  help="merge \"last message repeated x times\" with the event repeated")
	parser.add_option("-q", "--quiet", action="store_true", dest="quiet", default=False,
	                  help="don't output anything, unless there were unmatched lines")
	parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
	                  help="shows additional messages in case of error")

	(options, args) = parser.parse_args()

	if len(sys.argv) == 1:
		parser.error("no logfile specified")

	IGNORE_FILENAME = options.ignorefile
	STATE_FILENAME = options.statefile
	REPEAT = options.repeat
	QUIET = options.quiet
	DEBUG = options.debug

	ignore_pats = read_patterns(IGNORE_FILENAME)
	states = read_states(STATE_FILENAME)
	for filename in args:
		summarize(filename, states)
	save_states(STATE_FILENAME, states)

if __name__ == "__main__":
	main()

