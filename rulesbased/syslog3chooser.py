#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Gary Wright wrightsolutions.co.uk/contact

# ./syslog3chooser.py 'compare'

from datetime import datetime as dt
from sys import argv
import syslog3summary as sys3

def stateful(filename_stateful='/tmp/syslog',debug=True,class_word='Selecting'):
 
    summarized = sys3.summarize_stateful(filename_stateful,debug=True,class_word=class_word)
    linecount_selected = -1
    if summarized:
        try:
            linecount_selected = summarized.linecount_selected
        except AttributeError as e:
            pass
        print(summarized.linecount)
        if summarized.linecount > 0 or linecount_selected > 0:
            for rest in summarized.order_list:
                print("{0:8d} {1:s}".format(summarized.counts_dict[rest], rest),end='')
            print("{0:5d} items in counts dictionary - {1}".format(
                    len(summarized.counts_dict),'printing is dependent on order_list'))
            for repline in summarized.report_lines:
                print(repline)
    else:
        print('summarize_stateful not completed as expected')
    print("{0} Completed class_word={1} *** end ***".format(dt.now().isoformat(),class_word))

    return


def compare_stateful(filename_stateful='/tmp/syslog',debug=True,
                     class_word1='Selecting',class_word2='SelectingSimple'):
    print("{0} Comparing '{1}' and '{2}'".format(dt.now().isoformat(),
                                             class_word1,class_word2))
    stateful(filename_stateful,debug,class_word=class_word1)
    stateful(filename_stateful,debug,class_word=class_word2)
    return

if __name__ == "__main__":
    if len(argv) < 2:
        stateful(class_word='SelectingSimple')
    elif argv[1] == 'compare':
        compare_stateful()
    elif len(argv) > 2:
        compare_stateful(class_word1=argv[1],class_word2=argv[2])
    else:
        compare_stateful()
        
