#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Gary Wright wrightsolutions.co.uk/contact

import syslog3summary as sys3
 
#summarized = sys3.summarize_stateful('/var/log/syslog')
#Uncomment the above if you are root or have loose permissions on /var/log/syslog
summarized = sys3.summarize_stateful('/tmp/syslog')
#Comment out the above if you are instead using /var/log/syslog
if summarized:
    print((summarized.linecount))
    if summarized.linecount > 0:
        for repline in summarized.report_lines:
            print(repline)
        for rest in summarized.order_list:
            print("{0:8d} {1:s}".format(summarized.counts_dict[rest], rest),end='')
else:
    print('summarize_stateful not completed as expected')

