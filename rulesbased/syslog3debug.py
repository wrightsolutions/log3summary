#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Gary Wright wrightsolutions.co.uk/contact

import syslog3summary as sys3
 
summarized = sys3.summarize_stateful('/tmp/syslog',debug=True)
if summarized:
    print(summarized.linecount)
    if summarized.linecount > 0:
        for repline in summarized.report_lines:
            print(repline)
else:
    print('summarize_stateful not completed as expected')

