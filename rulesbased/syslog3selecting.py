#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Gary Wright wrightsolutions.co.uk/contact

import syslog3summary as sys3
 
summarized = sys3.summarize_selected('/tmp/syslog',debug=True)
if summarized:
    print(summarized.linecount_selected)
    print(summarized.linecount)
    if summarized.linecount > 0:
        for rest in summarized.order_list:
            print("{0:8d} {1:s}".format(summarized.counts_dict[rest], rest),end='')
        for repline in summarized.report_lines:
            print(repline)
else:
    print('summarize_selected not completed as expected')

